// ignore_for_file: prefer_const_constructors

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class BedPage extends StatefulWidget {
  @override
  State<BedPage> createState() => _BedPageState();
}

class _BedPageState extends State<BedPage> {
  TextEditingController _namecontroller = TextEditingController();
  TextEditingController _emailcontroller = TextEditingController();
  TextEditingController _isicontroller = TextEditingController();

  List listRs = [];
  void getRs() async {
    listRs.clear();
    String url = 'https://pbp-f08.herokuapp.com/rumahsakit/';
    var response = await http.get(Uri.parse(url));
    List responseBody = jsonDecode(response.body);

    for (var itemBody in responseBody) {
      if (itemBody['bed_ada']) {
        listRs.add(itemBody);
      }
    }
    setState(() {});
  }

  @override
  void initState() {
    // TODO: implement initState
    getRs();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFF10323A),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Selamat Datang,'),
              listRs.isEmpty
                  ? const Text("Tidak ada.")
                  : GridView.builder(
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2, childAspectRatio: 1),
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: listRs.length,
                      itemBuilder: (context, index) {
                        Map<String, dynamic> itemRs = listRs[index];
                        return Card(
                          child: Padding(
                            padding: const EdgeInsets.all(8),
                            child: Column(
                              children: [
                                Padding(
                                    padding: EdgeInsets.all(10.0),
                                    child: Text(
                                      itemRs['nama'],
                                    )),
                                Expanded(
                                  child: Image(
                                    image: AssetImage(
                                        'assets/images/medicine.png'),
                                    fit: BoxFit.cover,
                                    height: double.maxFinite,
                                    width: double.maxFinite,
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(10.0),
                                  child: ElevatedButton(
                                    onPressed: () {},
                                    child: Text("Detail"),
                                  ),
                                )
                              ],
                            ),
                          ),
                        );
                      }),
              Container(
                color: Colors.teal[900],
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.all(18),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Masukan",
                        style: TextStyle(
                            fontWeight: FontWeight.w900,
                            fontSize: 20,
                            color: Colors.white),
                      ),
                      SizedBox(
                        width: 200,
                        child: TextField(
                          controller: _emailcontroller,
                          decoration: InputDecoration(
                              labelText: "Email",
                              border: OutlineInputBorder(),
                              fillColor: Colors.white,
                              filled: true),
                          keyboardType: TextInputType.emailAddress,
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      SizedBox(
                        width: 200,
                        child: TextField(
                          controller: _namecontroller,
                          decoration: InputDecoration(
                              labelText: "Name",
                              border: OutlineInputBorder(),
                              fillColor: Colors.white,
                              filled: true),
                          keyboardType: TextInputType.emailAddress,
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      SizedBox(
                        width: 200,
                        child: TextField(
                          minLines: 5,
                          maxLines: 10,
                          controller: _isicontroller,
                          decoration: InputDecoration(
                              labelText: "isi",
                              border: OutlineInputBorder(),
                              fillColor: Colors.white,
                              filled: true),
                          keyboardType: TextInputType.emailAddress,
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      ElevatedButton(
                          onPressed: () {
                            print("Name: " + _emailcontroller.text);
                            print("Name: " + _namecontroller.text);
                            print("Name: " + _isicontroller.text);
                          },
                          child: Text("Submit"))
                    ],
                  ),
                ),
              )
            ],
          ),
        ));
  }
}
