import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFF10323A),
        body: Column(
          children: <Widget>[
            Flexible(
              flex: 2,
              child: Container(
                child: Stack(
                  children: <Widget>[
                    Positioned(
                      bottom: 23,
                      top: 0,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.elliptical(30, 30),
                            bottomRight: Radius.elliptical(30, 30),
                          ),
                          /*gradient: LinearGradient(
                              colors: [
                                Color(0xFF10323A),
                                Color(0xFF10323A),
                              ],
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight),*/
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            //Icon(Icons.menu, color: Colors.white),
                            Spacer(),
                            RichText(
                              text: TextSpan(
                                style: TextStyle(letterSpacing: 1.3),
                                children: [
                                  TextSpan(
                                    text: "Welcome",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 25,
                                      color: Colors.white,
                                    ),
                                  ),
                                  TextSpan(
                                    text: " to",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 25,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 9,
                            ),
                            Text(
                              "HERMITAGE",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                                fontSize: 25,
                                letterSpacing: 1.3,
                              ),
                            ),
                            //Spacer(),
                          ],
                        ),
                      ),
                    ),
                    /*Positioned(
                      bottom: 0,
                      left: 20,
                      right: 20,
                      child: Container(
                        height: 57,
                        alignment: Alignment.center,
                        /*child: Center(
                          child: TextField(
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(15.0),
                                borderSide: BorderSide.none
                              ),
                              hintText: "Search Furniture",
                              prefixIcon: Icon(
                                Icons.search,
                                color: Colors.blue,
                              ),
                              fillColor: Colors.white,
                              filled: true,
                            ),
                          ),
                        ),*/
                      ),
                    )*/
                  ],
                ),
              ),
            ),
            Flexible(
              flex: 5,
              child: Container(
                padding: EdgeInsets.all(15),
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      CategoryContainer(),
                      SizedBox(
                        height: 9,
                      ),
                      Divider(),
                      SizedBox(
                        height: 9,
                      ),
                      DealsContainer(),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      
        
    );
}
}

class CategoryContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          /*Row(
            
            children: <Widget>[
              Text(
                "Shop for",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 19),
              ),
              /*Spacer(),
              FlatButton(
                child: Text(
                  "See All",
                  style: TextStyle(color: Colors.blueAccent, fontSize: 15),
                ),
                onPressed: () {},
              )*/
            ],
          ),*/
          SizedBox(
            height: 9,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Flexible(
                child: Column(
                  children: <Widget>[
                    Image.asset("assets/images/profile.png", width: 50),
                    SizedBox(height: 15),
                    Text("Profil")
                  ],
                ),
              ),
              Flexible(
                child: Column(
                  children: <Widget>[
                    Image.asset("assets/images/login.png", width: 50),
                    SizedBox(height: 15),
                    Text("Login")
                  ],
                ),
              ),
              Flexible(
                child: Column(
                  children: <Widget>[
                    Image.asset("assets/images/register.png", width: 50),
                    SizedBox(height: 15),
                    Text("Register")
                  ],
                ),
              ),
              Flexible(
                child: Column(
                  children: <Widget>[
                    Image.asset("assets/images/newspaper.png", width: 50),
                    SizedBox(height: 15),
                    Text("Artikel")
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class DealsContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, '/details');
      },
      child: Container(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(
                  "More Information",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 19),
                ),
                Spacer(),
                /*FlatButton(
                  child: Text(
                    "See All",
                    style: TextStyle(color: Colors.blueAccent, fontSize: 15),
                  ),
                  onPressed: () {},
                )*/
              ],
            ),
            SizedBox(
              height: 9,
            ),
            Container(
              height: 171,
              child: ListView.builder(
                itemCount: 3,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, id) {
                  return SingleItem();
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}

class SingleItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 251,
      margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15.0),
        boxShadow: [
          BoxShadow(color: Colors.grey, blurRadius: 3.0),
        ],
      ),
      child: Stack(
        children: <Widget>[
          Positioned(
            bottom: 0,
            right: 0,
            child: Container(
              child: Image.asset(
                "assets/images/vax.jpg",
                //width: 121,
              ),
            ),
          ),
          /*  Positioned(
            bottom: 0,
            right: 0,
            child: Container(
              child: Image.asset(
                "assets/images/bed.jpg",
                width: 121,
              ),
            ),
          ),*/
          Container(
            padding: EdgeInsets.all(15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Chairs Starting from",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(
                  "\$ 39.99",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Spacer(),
                Text(
                  "Ends in 02:00:25",
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

/*
Template : https://cybdom.tech/furniture-app-ui-in-flutter-source-code/
*/