class RumahSakit {
  String nama;
  String phone;
  String alamat;
  String image;
  int jumlahVaksin;
  int jumlahBed;

  // Constructor
  RumahSakit(
      {required this.image,
      required this.nama,
      required this.phone,
      required this.alamat,
      required this.jumlahBed,
      required this.jumlahVaksin});

  RumahSakit copy(
          {String? imagePath,
          String? nama,
          String? phone,
          String? alamat,
          int? jumlahVaksin,
          int? jumlahBed}) =>
      RumahSakit(
          image: imagePath ?? this.image,
          nama: nama ?? this.nama,
          alamat: alamat ?? this.alamat,
          phone: phone ?? this.phone,
          jumlahVaksin: jumlahVaksin ?? this.jumlahVaksin,
          jumlahBed: jumlahBed ?? this.jumlahBed);

  static RumahSakit fromJson(Map<String, dynamic> json) => RumahSakit(
      image: json['imagePath'],
      nama: json['nama'],
      phone: json['phone'],
      alamat: json['alamat'],
      jumlahBed: json['jumlahBed'],
      jumlahVaksin: json['jumlahVaksin']);

  Map<String, dynamic> toJson() => {
        'imagePath': image,
        'nama': nama,
        'alamat': alamat,
        'phone': phone,
        'jumlahBed': jumlahBed,
        'jumlahVaksin': jumlahVaksin
      };
}
