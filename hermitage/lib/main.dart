import 'package:flutter/material.dart';
import 'package:hermitage/profile_user.dart';
import 'artikel.dart';
import 'vaksin.dart';
import 'bed.dart';
import 'home.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Hermitage',
      theme: ThemeData(
        brightness: Brightness.dark,
        primarySwatch: Colors.blueGrey,
      ),
      home: const MyHomePage(title: 'HERMITAGE'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _formKey = GlobalKey<FormState>();
  int _counter = 0;
  int _currentIndex = 0;
  static List<Widget> _halaman = <Widget>[
    HomePage(),
    BedPage(),
    VaksinPage(),
    ArtikelPage(),
    ProfileApp(),
  ];

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideDrawer(),
      backgroundColor: Color(0xFF10323A),
      appBar: AppBar(
        backgroundColor: Color(0xFF020215),
        title: TextButton(
            onPressed: () {},
            child: Row(
              children: [
                Container(
                  alignment: Alignment.center, // use aligment
                  child: Image.asset("assets/images/icon.png",
                      height: 30, width: 30, fit: BoxFit.cover),
                ),
                const SizedBox(
                  width: 10.0,
                ),
                const Text(
                  'HERMITAGE',
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ],
            )),
      ),
      body: _halaman[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        onTap: onTabTapped,
        currentIndex: _currentIndex,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.house),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.airline_seat_individual_suite),
            label: 'Bed',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.assignment),
            label: 'Vaksin',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.book),
            label: 'Artikel',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.corporate_fare_rounded),
            label: 'Profil',
          ),
        ],
      ),
    );
  }
}

class SideDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          DrawerHeader(
            child: Center(
              child: Text(
                'Mau Lihat Apa, Ges?',
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white, fontSize: 25),
              ),
            ),
            decoration: BoxDecoration(
              color: Color(0xFF10323A),
            ),
          ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text('Home'),
            onTap: () => {},
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text('Register'),
            // onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: Icon(Icons.login_sharp),
            title: Text('Login'),
            onTap: () => {Navigator.of(context).pop()},
          ),
        ],
      ),
    );
  }
}

